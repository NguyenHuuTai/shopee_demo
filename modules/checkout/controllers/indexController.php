<?php

require_once "admin/MongoDB/vendor/autoload.php";

function construct() {
    load_model('index');
    load('helper', 'format');
}

function indexAction() {

    $list_buy = get_list_buy_cart();
    $num_order = get_num_order_cart();
    $sub_total = get_total_cart();
    $data['list_buy'] = $list_buy;
    $data['num_order'] = $num_order;
    $data['sub_total'] = $sub_total;
    $info_user = get_user_by_username(user_login());
    $data['info_user'] = $info_user;
    load_view('index', $data);
}

function deleteAction() {
    
}

function addAction() {
    if (!is_login()) {
        redirect('?mod=users&action=regLogin');
    } else {
        $id = $_GET['id'];
        add_cart($id);
        redirect('?mod=checkout&controller=index');
    }
}

function updateAction() {
    $list_buy = get_list_buy_cart();
    $num_order = get_num_order_cart();
    $sub_total = get_total_cart();
    if (isset($_POST['btn-order'])){
        echo $sub_total;
    }

}

function delete_allAction() {
    
}
